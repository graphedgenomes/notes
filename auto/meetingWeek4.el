(TeX-add-style-hook
 "meetingWeek4"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("report" "a4paper" "11pt" "english")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("fontenc" "T1") ("inputenc" "utf8")))
   (TeX-run-style-hooks
    "latex2e"
    "report"
    "rep11"
    "fontenc"
    "inputenc"
    "amsmath")))

