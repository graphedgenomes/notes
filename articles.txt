

Grafrepresentasjon av genomet
	(Starte med å kategorisere de vi har lest til nå, med noen stikkord/notater)
	- Assembly/mapping
	- MSA
	- Referansegenom (teorien bak grafrepresentasjon som referanse)
	

Analyser/problemer løst ved hjelp av grafer
	(Se hva vi finner)


Metoder brukt innen andre fagområder som kan "portes"
	- Maskinlæring
	- Deep learning (eksempel: word2vec)
	- Representere tekst i grafer


Bioinformatikk: Typiske problemstillinger/analyser
	(Finne artikler som gir oversikt over nåværende aktuelle problemstillinger. Sjekke de GK sendte.)
