(TeX-add-style-hook
 "project-description-knut"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("report" "a4paper" "11pt" "english")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("fontenc" "T1") ("inputenc" "utf8") ("biblatex" "backend=biber" "maxbibnames=10" "minbibnames=1" "sorting=none")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "background"
    "review"
    "tracksOnGraphs"
    "epigenomic-comparison"
    "phylogeneticInferrence"
    "cancerGenomeGraphs"
    "report"
    "rep11"
    "fontenc"
    "inputenc"
    "biblatex")
   (TeX-add-symbols
    "ivar")
   (LaTeX-add-bibliographies
    "../references")))

