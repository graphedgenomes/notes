Project 2, Todo:
- Implementet SegmentCollection and Segment for various segment types
- Implement overlap method in SegmentCollection
- Manage to load the whole human and mouse genomes into the graph
- Possible analysis: Check overlap between points/segments in mouse/human in the graph, and compare with results from USCS genome browser
